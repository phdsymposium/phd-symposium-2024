// Modal data
const modalsData = [
  {
    id: "modal-mason",
    title: "Christopher E. Mason (he/him)",
    body: "Christopher Mason, PhD is a Professor of Genomics, Physiology, Biophysics, and Neuroscience at Weill Cornell Medicine and Director of the WorldQuant Initiative for Quantitative Prediction. He completed a dual B.S. in Genetics & Biochemistry at University of Wisconsin-Madison (2001), a Ph.D. in Genetics at Yale University (2006), Clinical Genetics Fellowship at Yale Medical School (2009), and was the Visiting Fellow of Genomics, Ethics, and Law at Yale Law School (2006-2009). His laboratory creates and deploys new technologies and algorithms for medicine, integrative omics, and cell/genome engineering, spanning >350 peer-reviewed papers, five patents, five diagnostics tests, 10 biotechnology companies, and 4 non-profits. Dr. Mason also holds affiliate faculty appointments at the New York Genome Center, Yale Law School, and the Consortium for Space Genetics at Harvard Medical School. He is the author of The Next 500 Years: Engineering Life to Reach New Worlds and The Age of Prediction.",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },
  {
    id: "modal-yoo",
    title: "Esther Yoo (she/her)",
    body: "I am the best head of Website",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"},
  {
    id: "modal-batisti",
    title: "Nicolo Batisti (he/him)",
    body: "Best Chef",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },

];

// Create modals dynamically
for (let i = 0; i < modalsData.length; i++) {
  const modalData = modalsData[i];

  // Create modal element
  const modalElement = document.createElement("div");
  modalElement.classList.add("modal", "fade");
  modalElement.id = modalData.id;
  modalElement.setAttribute("data-bs-keyboard", "false");
  modalElement.setAttribute("tabindex", "-1");
  modalElement.setAttribute("aria-labelledby", "modalLabel-" + i);
  modalElement.setAttribute("aria-hidden", "true");

  // Modal dialog
  const modalDialog = document.createElement("div");
  modalDialog.classList.add("modal-dialog", "modal-dialog-centered", "modal-dialog-scrollable");

  // Modal content
  const modalContent = document.createElement("div");
  modalContent.classList.add("modal-content");

  // Modal header
  const modalHeader = document.createElement("div");
  modalHeader.classList.add("modal-header");
  modalHeader.style.backgroundColor = modalData.headerColor;

  const modalTitle = document.createElement("h5");
  modalTitle.classList.add("modal-title");
  modalTitle.id = "modalLabel-" + i;
  modalTitle.textContent = modalData.title;
  modalTitle.style.borderColor = modalData.borderColor;
  modalTitle.style.borderBottomColor = modalData.borderColor;

  modalHeader.appendChild(modalTitle);

  // Modal body
  const modalBody = document.createElement("div");
  modalBody.classList.add("modal-body");
  modalBody.textContent = modalData.body;

  modalContent.appendChild(modalHeader);
  modalContent.appendChild(modalBody);

  modalDialog.appendChild(modalContent);

  modalElement.appendChild(modalDialog);

  // Append modal to the document body
  document.body.appendChild(modalElement);
}
