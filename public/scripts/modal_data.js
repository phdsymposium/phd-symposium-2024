// Modal data
const modalsData = [
  {
    id: "modal-mason",
    title: "Christopher E. Mason (he/him)",
    body: "Christopher Mason, PhD is a Professor of Genomics, Physiology, Biophysics, and Neuroscience at Weill Cornell Medicine and Director of the WorldQuant Initiative for Quantitative Prediction. He completed a dual B.S. in Genetics & Biochemistry at University of Wisconsin-Madison (2001), a Ph.D. in Genetics at Yale University (2006), Clinical Genetics Fellowship at Yale Medical School (2009), and was the Visiting Fellow of Genomics, Ethics, and Law at Yale Law School (2006-2009). His laboratory creates and deploys new technologies and algorithms for medicine, integrative omics, and cell/genome engineering, spanning >350 peer-reviewed papers, five patents, five diagnostics tests, 10 biotechnology companies, and 4 non-profits. Dr. Mason also holds affiliate faculty appointments at the New York Genome Center, Yale Law School, and the Consortium for Space Genetics at Harvard Medical School. He is the author of The Next 500 Years: Engineering Life to Reach New Worlds and The Age of Prediction.",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },
  {
    id: "modal-bernheim",
    title: "Aude Bernheim (she/her)",
    body: "All cells, eukaryotic and prokaryotic, face the threat of viral infections and develop anti-viral systems to fight them off. The constant arms race between cells and their viruses leads to the diversification of these defense systems. Thus, hallmarks of immunity in diverse domains such as plants, animals or bacteria are completely different. However, the unveiling of novel anti-phage defense systems in prokaryotes lead us to revisit this paradigm. I study bacterial immunity and how the conservation of immune modules across domains of life can lead to discoveries in prokaryotes and eukaryotes.",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },
  {
    id: "modal-shahbazi",
    title: "Marta Shahbazi (she/her)",
    body: "The overarching aim of my lab is to dissect the molecular mechanisms that regulate cell fate decisions during mammalian embryo development, under both physiological and pathological conditions. Most studies of mammalian development have focused on dissecting the epigenetic and transcriptional control of cell fate specification. However, cell fate decisions need to be tightly coordinated with changes in tissue shape to ensure that the right fate decision is taken at the right place and time. This tight coordination between cell fate and tissue morphogenesis is at the core of developmental self-organisation, the process by which cells organise themselves in high-order structures and patterns. Cells sense physical, chemical, and geometrical cues, and adapt their decisions accordingly, which leads to developmental plasticity, the ability to react to a dynamic environment by altering the cellular decision-making process. To dissect the mechanisms that underlie self-organisation and plasticity during mammalian development, we focus on the stage of implantation. This is a critical developmental period when dramatic changes in gene expression, cell organisation, and overall embryo patterning and morphology take place, but it is poorly understood due to the inaccessibility of the embryo. Our experimental approach is based on the development of new stem cell and embryo culture methodologies in combination with state-of-the-art transcriptomics, proteomics, imaging, and functional in vivo experiments",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },
  {
    id: "modal-urban",
    title: "Lara Urabn (she/her)",
    body: "Portable real-time genomics through nanopore sequencing has started to transform molecular biology into a frontline strategy to tackle One Health issues at the intersection of environmental, animal, and global health. I will show several of such One Health applications of nanopore sequencing technology (Urban et al., 2023); I will specifically focus on (1) how in situ nanopore metagenomics can be leveraged to describe the air microbiome of natural and polluted habitats, including the presence and function of biodegrading microorganisms (Reska et al., 2023) and the transmission of zoonotic pathogens (Perlas et al., 2024), and on how (2) unique characteristics of nanopore sequencing in combination with tailored AI algorithms allow us to robustly assess antibiotic resistance and virulence features of pathogens (Sauerborn et al., 2023).",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },
  {
    id: "modal-guell",
    title: "Marc Güell (he/him)",
    body: "Our research is focused on developing new principles and technologies to engineer biological systems. AI is revolutionising biodesign, and its coupling with multiplex DNA writing enables a new concept of synthetic evolution, which promises to accelerate synthetic biology. First, I will present our vision of how we are leveraging bioprospecting, AI, and evolution to create new biological functions. Then, I will focus on 1) how we developed a gene writer for human cells and its deployment ex vivo (T cells) and in vivo (liver targeting). 2) I will present how we use Cutibacterium acnes as a chassis to engineer human skin properties.",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },
  {
    id: "modal-",
    title: "Name (she/her)",
    body: "abstract",
    headerColor: "rgba(43,112,147,255)",
    borderColor: "rgba(43,112,147,255)"
  },


];

// Create modals dynamically
for (let i = 0; i < modalsData.length; i++) {
  const modalData = modalsData[i];

  // Create modal element
  const modalElement = document.createElement("div");
  modalElement.classList.add("modal", "fade");
  modalElement.id = modalData.id;
  modalElement.setAttribute("data-bs-keyboard", "false");
  modalElement.setAttribute("tabindex", "-1");
  modalElement.setAttribute("aria-labelledby", "modalLabel-" + i);
  modalElement.setAttribute("aria-hidden", "true");

  // Modal dialog
  const modalDialog = document.createElement("div");
  modalDialog.classList.add("modal-dialog", "modal-dialog-centered", "modal-dialog-scrollable");

  // Modal content
  const modalContent = document.createElement("div");
  modalContent.classList.add("modal-content");

  // Modal header
  const modalHeader = document.createElement("div");
  modalHeader.classList.add("modal-header");
  modalHeader.style.backgroundColor = modalData.headerColor;

  const modalTitle = document.createElement("h5");
  modalTitle.classList.add("modal-title");
  modalTitle.id = "modalLabel-" + i;
  modalTitle.textContent = modalData.title;
  modalTitle.style.borderColor = modalData.borderColor;
  modalTitle.style.borderBottomColor = modalData.borderColor;

  modalHeader.appendChild(modalTitle);

  // Modal body
  const modalBody = document.createElement("div");
  modalBody.classList.add("modal-body");
  modalBody.textContent = modalData.body;

  modalContent.appendChild(modalHeader);
  modalContent.appendChild(modalBody);

  modalDialog.appendChild(modalContent);

  modalElement.appendChild(modalDialog);

  // Append modal to the document body
  document.body.appendChild(modalElement);
}
