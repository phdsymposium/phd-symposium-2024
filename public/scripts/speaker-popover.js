$(document).ready(function() {
    $('#mason-div').popover({
      placement: 'bottom',
      title: 'Christopher E. Mason (he/him)',
      content: 'Christopher Mason, PhD is a Professor of Genomics, Physiology, Biophysics, and Neuroscience at Weill Cornell Medicine and Director of the WorldQuant Initiative for Quantitative Prediction. He completed a dual B.S. in Genetics & Biochemistry at University of Wisconsin-Madison (2001), a Ph.D. in Genetics at Yale University (2006), Clinical Genetics Fellowship at Yale Medical School (2009), and was the Visiting Fellow of Genomics, Ethics, and Law at Yale Law School (2006-2009). His laboratory creates and deploys new technologies and algorithms for medicine, integrative omics, and cell/genome engineering, spanning >350 peer-reviewed papers, five patents, five diagnostics tests, 10 biotechnology companies, and 4 non-profits. Dr. Mason also holds affiliate faculty appointments at the New York Genome Center, Yale Law School, and the Consortium for Space Genetics at Harvard Medical School. He is the author of The Next 500 Years: Engineering Life to Reach New Worlds and The Age of Prediction.',
      trigger: 'click',
      template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
    });

    // Hide popover when clicking outside or on another popover
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        // hide any open popovers when anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    // popover z-index fix under the navbar
    $('.popover').css('z-index', '100');
    });
});

$(document).ready(function() {
  $('#bernheim-div').popover({
    placement: 'bottom',
    title: 'Aude Bernheim (she/her)',
    content: 'All cells, eukaryotic and prokaryotic, face the threat of viral infections and develop anti-viral systems to fight them off. The constant arms race between cells and their viruses leads to the diversification of these defense systems. Thus, hallmarks of immunity in diverse domains such as plants, animals or bacteria are completely different. However, the unveiling of novel anti-phage defense systems in prokaryotes lead us to revisit this paradigm. I study bacterial immunity and how the conservation of immune modules across domains of life can lead to discoveries in prokaryotes and eukaryotes.',
    trigger: 'click',
    template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  });

  // Hide popover when clicking outside or on another popover
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      // hide any open popovers when anywhere else in the body is clicked
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      }
    });
  // popover z-index fix under the navbar
  $('.popover').css('z-index', '100');
  });
});

$(document).ready(function() {
  $('#urban-div').popover({
    placement: 'bottom',
    title: 'Lara Urban (she/her)',
    content: 'Portable real-time genomics through nanopore sequencing has started to transform molecular biology into a frontline strategy to tackle One Health issues at the intersection of environmental, animal, and global health. I will show several of such One Health applications of nanopore sequencing technology (Urban et al., 2023); I will specifically focus on (1) how in situ nanopore metagenomics can be leveraged to describe the air microbiome of natural and polluted habitats, including the presence and function of biodegrading microorganisms (Reska et al., 2023) and the transmission of zoonotic pathogens (Perlas et al., 2024), and on how (2) unique characteristics of nanopore sequencing in combination with tailored AI algorithms allow us to robustly assess antibiotic resistance and virulence features of pathogens (Sauerborn et al., 2023).',
    trigger: 'click',
    template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  });


  // Hide popover when clicking outside or on another popover
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      // hide any open popovers when anywhere else in the body is clicked
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      }
    });
  // popover z-index fix under the navbar
  $('.popover').css('z-index', '100');
  });
});

$(document).ready(function() {
  $('#guell-div').popover({
    placement: 'bottom',
    title: 'Marc Güell (he/him)',
    content: 'Our research is focused on developing new principles and technologies to engineer biological systems. AI is revolutionising biodesign, and its coupling with multiplex DNA writing enables a new concept of synthetic evolution, which promises to accelerate synthetic biology. First, I will present our vision of how we are leveraging bioprospecting, AI, and evolution to create new biological functions. Then, I will focus on 1) how we developed a gene writer for human cells and its deployment ex vivo (T cells) and in vivo (liver targeting). 2) I will present how we use Cutibacterium acnes as a chassis to engineer human skin properties.',
    trigger: 'click',
    template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  });

  
  // Hide popover when clicking outside or on another popover
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      // hide any open popovers when anywhere else in the body is clicked
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      }
    });
  // popover z-index fix under the navbar
  $('.popover').css('z-index', '100');
  });
});

$(document).ready(function() {
  $('#shahbazi-div').popover({
    placement: 'bottom',
    title: 'Marta Shahbazi (she/her)',
    content: 'The overarching aim of my lab is to dissect the molecular mechanisms that regulate cell fate decisions during mammalian embryo development, under both physiological and pathological conditions. Most studies of mammalian development have focused on dissecting the epigenetic and transcriptional control of cell fate specification. However, cell fate decisions need to be tightly coordinated with changes in tissue shape to ensure that the right fate decision is taken at the right place and time. This tight coordination between cell fate and tissue morphogenesis is at the core of developmental self-organisation, the process by which cells organise themselves in high-order structures and patterns. Cells sense physical, chemical, and geometrical cues, and adapt their decisions accordingly, which leads to developmental plasticity, the ability to react to a dynamic environment by altering the cellular decision-making process. To dissect the mechanisms that underlie self-organisation and plasticity during mammalian development, we focus on the stage of implantation. This is a critical developmental period when dramatic changes in gene expression, cell organisation, and overall embryo patterning and morphology take place, but it is poorly understood due to the inaccessibility of the embryo. Our experimental approach is based on the development of new stem cell and embryo culture methodologies in combination with state-of-the-art transcriptomics, proteomics, imaging, and functional in vivo experiments.', 
    trigger: 'click',
    template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  });

  
  // Hide popover when clicking outside or on another popover
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      // hide any open popovers when anywhere else in the body is clicked
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      }
    });
  // popover z-index fix under the navbar
  $('.popover').css('z-index', '100');
  });
});

$(document).ready(function() {
  $('#placeholder-div').popover({
    placement: 'bottom',
    title: 'Name (she/her)',
    content: 'abstract',
    trigger: 'click',
    template: '<div class="popover speaker-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  });

  // Hide popover when clicking outside or on another popover
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      // hide any open popovers when anywhere else in the body is clicked
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');
      }
    });
  // popover z-index fix under the navbar
  $('.popover').css('z-index', '100');
  });
});